
	let config = {
			id : {
				logout_id : document.getElementById("logout_id"),
				page_container : document.getElementById("page_container"),
				person_name : document.getElementById("person_name"),
				tracker_add : document.getElementById("tracker-add-form"),
				tracker_succ : document.getElementById("tracker-code-snippet"),
				tracker_html_content : document.getElementById("tracker-codesnippet-card-code"),
				card_title : document.getElementById("tracker-codesnippet-card-title")

			},
			classes : {
				listViewHolder : document.getElementsByClassName('listViewHolder')
			}
	};
        
        function showActivity(activityId){
            var elems = document.getElementsByClassName("page-activity");
            for( var i=0; i<elems.length; i++){
                elems[i].style.display = "none";
            }
            document.getElementById(activityId).style.display = "block";
        }

        
        var list_item = function(data){
            return `<div class="list-item" id="tracker-${data.id}">
            <div class="list-index ${data.isRead ? 'read-icon' : 'unread-icon' }">
                <i class="material-icons md-24">
                        ${data.isRead ? 'done_all' : 'visibility_off' }
                </i>
            </div>
            <div class="list-cont-type">
                ${data.serviceType}
            </div>
            <div class="list-content" onclick="showTrackerAccessHsitory('${data.id}', '${data.name}', '${data.serviceType}')" >
                ${data.name}
            </div>
            <div class="list-actions">
                <div class="list-action-item snippet" data-id="${data.id}" data-name="${data.name}" title="Code Snippet" >
                    <i class="material-icons" data-id="${data.id}" data-name="${data.name}">code</i>
                </div>
                <div class="list-action-item delete" title="Delete" data-id="${data.id}" data-name="${data.name}" >
                    <i class="material-icons" data-id="${data.id}" data-name="${data.name}" >delete_forever</i>
                </div>
            </div>
            <div class="list-time">
                ${new Date(data.modifiedTime).toLocaleString()}
            </div>
            <div class="list-expand">
                <div class="tracker-html-content">
                    <code>&lt;img src="${data.img_url}"/&gt;</code>
                </div>
            </div>
            </div>`;
        };


	var _UA_FIREWALL_KEY = null;
	const BASE_URL = window.location.origin.indexOf('localhost') < 0 ? "https://read.ulgebra.com" : "http://localhost:8080";
	const _UA_DOMAIN = BASE_URL + "/api/v1";
	var staticHTTP = null;
	var userId = null;
	var trackersCount = 0;
	var usageCount = 0;

	function getToken(e){
	     e.preventDefault();
	    var object = {};
	    formData = new FormData(e.target);
	    formData.forEach((value, key) => {object[key] = value;});
	    
	    let signIN_hdr = Object.assign({},object);
	    object = {};
	    object.scopes = ["ADMIN_ACCESS"];
	    var json = JSON.stringify(object);
	    
	    new HTTPUtil().httpPOST("/authtokens", setFIREWALLCookie, function(){
	    	document.getElementById("signin-error").style.display = "block";
	    },
	    json,signIN_hdr);
	    return false;
	}

	function signUp(e){
	    e.preventDefault();
	   var object = {};
	   formData = new FormData(e.target);
	   formData.forEach((value, key) => {object[key] = value;});
	   var json = JSON.stringify(object);
	   new HTTPUtil().httpPOST("/users", setFIREWALLCookie, function(){
	    	document.getElementById("signup-error").style.display = "block";
	    }, json);
	   return false;
	}
	
	function deleteAPIKeyUsage(usageId){
		new HTTPUtil().httpDELETE('/apikeyUsage/'+usageId, function(){
			
			document.getElementById("apikeyusage-"+usageId).remove();
			
		},function(){alert('try again')});
		console.log(2);
	}

	function createTracker(e){
		e.preventDefault();
		let uid = ulgebra.get_cookie('userid');
		if( ! uid ) return;
		var object = {};
		formData = new FormData(e.target);
		formData.forEach((value, key) => {object[key] = value;});
		var json = JSON.stringify(object);
		new HTTPUtil().httpPOST("/trackers", function(resp){
			if( resp.id && resp.id.length ){
				config.id.tracker_succ.style.display = "block"; 
				config.id.card_title.innerText = resp.name;
				let img_tok = '<code>&lt;img src="'+BASE_URL+'/public/read/u/'+uid+'/t/'+resp.id+'"/&gt;</code>';
				config.id.tracker_html_content.innerHTML = img_tok;
                document.getElementById("tracker-code-popup").style.display = "block";
                showActivity('activity-tracker-list');
                renderTrackerItemOnTop(resp);
                ulgebra.set_trackers_actions();
				
			}
		}, console.log, json);
		return false;
	}

	function setFIREWALLCookie(data){
		let elem = config.classes.listViewHolder[0];
		elem.innerHTML = "";
	    let token = data.token.token;
	    if(token){
	    	var cookieUtil = new CookieUtil();
                cookieUtil.setCookie('usertoken',  token  ,90);
                cookieUtil.setCookie('userid',   data.user.id  ,90);
                cookieUtil.setCookie('username',  data.user.lastName  ,90);
		cookie_page_changes();
		ulgebra.get_trackers();
	    }  
	}
        
        function showCreateNewTrackerActivity(){
            showActivity("activity-tracker-new");
            document.getElementById("tracker-add-form").style.display = "inline-block";
        }
        
        function showSignInActivity(){
        	document.getElementById("signin-error").style.display = "none";
        	showActivity('activity-signin');
        }
        
        function showSignUpActivity(){
        	document.getElementById("signup-error").style.display = "none";
        	showActivity('activity-signup');
        }

	class HTTPUtil{
	    httpGETCustom(url, onSuccess, onFailure, headers) {
	        var constructedHeader = new Headers(headers);
	        return this.httpExecute(url, this.httpSettings("GET", constructedHeader), onSuccess, onFailure);
	    }
	    httpGET(url, onSuccess, onFailure,header_object = null) {
	        return this.httpExecute(url, this.httpSettings("GET", this.httpHeaders(header_object)), onSuccess, onFailure);
	    }
	    httpPOST(url, onSuccess, onFailure, data,header_object = null) {
	        return this.httpExecute(url, this.httpSettings("POST", this.httpHeaders(header_object), data), onSuccess, onFailure);
	    }
	    httpPATCH(url, onSuccess, onFailure, data) {
	        return this.httpExecute(url, this.httpSettings("PATCH", this.httpHeaders(), data), onSuccess, onFailure);
	    }
	    httpDELETE(url, onSuccess, onFailure) {
	        return this.httpExecute(url, this.httpSettings("DELETE", this.httpHeaders()), onSuccess, onFailure);
	    }
	    httpHeaders(ext_object = null) {
	    	
	    	let header_object = {
		            "Authorization": _UA_FIREWALL_KEY,
		            "Content-Type": "application/json"
		    };
	    	
	    	if(ext_object != null){
	    		if(ext_object.hasOwnProperty('username'))
	    				delete header_object.Authorization;
	    		header_object = Object.assign({},header_object,ext_object);
	    	}
	        return (new Headers(header_object));
	    }
	    httpSettings(method, headers, data = "") {
	        var settingsObj = {
	            method: method,
	            headers: headers
	        };
	        if (method === "POST" || method === "PATCH" || method === "PUT") {
	            settingsObj.body = data;
	        }
	        return settingsObj;
	    }
	    httpExecute(url, http_settings, onSuccess, onFailure) {
	        url = _UA_DOMAIN+url;
	        var api_response;
	        var returnResult=null;
	        return fetch(url, http_settings).then(function (response) {
	        	
	            api_response = response;
	            if (response.ok) {
	            	if(response.status==204){
	            		return null;
	            	}
	                return response.json();
	            }
	            throw new Error("Request Not Successful");
	        }).then(function (result) {
	            returnResult= result;
	            onSuccess(result);
	        }).catch(function (error) {
//	            debugTraceNative(api_response, api_response.ok);
	            if( ! api_response.ok || http_settings.method==="DELETE"){
	                console.log(api_response);
	            }
	            
	            onFailure(error);
	            //debugTraceNative(api_response, api_response.ok);
	        });
	        
	    }
	}

	class CookieUtil{
	    setCookie(name,value,days) {
	        var expires = "";
	        if (days) {
	            var date = new Date();
	            date.setTime(date.getTime() + (days*24*60*60*1000));
	            expires = "; expires=" + date.toUTCString();
	        }
	        document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	    }
	    getCookie(name) {
	        var nameEQ = name + "=";
	        var ca = document.cookie.split(';');
	        for(var i=0;i < ca.length;i++) {
	            var c = ca[i];
	            while (c.charAt(0)==' ') c = c.substring(1,c.length);
	            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	        }
	        return null;
	    }
	    eraseCookie(name) {   
	        document.cookie = name+'=; Max-Age=-99999999;';  
	    }
	}

	function closeWindowPopUp(id){
		document.getElementById(id).style.display = 'none';
	}
        
        function showTrackerAccessHsitory(trackerId, trackerName, serviceType){
        	showActivity('activity-tracker-analysis');
                var readPinElem = document.getElementById('tracker-history-items-read-pinned');
                var elem = document.getElementById('tracker-history-items');
                document.getElementById('acitivity-history-subtitle-servicetype').innerHTML = serviceType;
                elem.innerHTML = "";
                readPinElem.innerHTML = "";
        	document.getElementById("acitivity-history-subtitle").innerHTML = (trackerName);
            new HTTPUtil().httpGET("/trackers/"+trackerId+"/history", function(data){
            	if(data == null){
            		return;
            	}
                var i = 1;
                data.data.reverse().forEach((hisItem)=>{
                    var elemHTML =  getHistoryItemFromTemplate(hisItem, i++);
                    elem.insertAdjacentHTML('afterbegin', elemHTML);
                    if(hisItem.UA_RR_MARK_READ){
                        readPinElem.insertAdjacentHTML('afterbegin', elemHTML);
                    }
                });
            }, alert);
	   return false;
        }
        
        function showAPIKeyUsage(){
        	showActivity('activity-apikey-usage-list');
        	var elem = document.getElementById('apikey-usage-items');
        	elem.innerHTML = "";
        	document.getElementById("apikey-value").innerHTML = (ulgebra.get_cookie('userid'));
        	new HTTPUtil().httpGET("/../../public/api/v1/apikey/"+ulgebra.get_cookie('userid')+"/usage", function(data){
        		if(data == null){
            		return;
            	}
        		var i = 1;
        		data.data.reverse().forEach((hisItem)=>{
        			var elemHTML =  getAPIKeyUsageItemFromTemplate(hisItem, i++);
        			elem.insertAdjacentHTML('afterbegin', elemHTML);
        			if(hisItem.UA_RR_MARK_READ){
        				readPinElem.insertAdjacentHTML('afterbegin', elemHTML);
        			}
        		});
        	}, alert);
        	return false;
        }
        
        function toggleAPIKey(){
            elem = document.getElementById("account-apikey-value");
            if(elem.style.display === "inline-block"){
                elem.innerHTML = "";
                elem.style.display = 'none';
            }
            else{
                elem.innerHTML = ulgebra.get_cookie('userid');
                elem.style.display = 'inline-block';
            }
        }

	function cookie_page_changes(){
                var cookieUtil = new CookieUtil();
		let token = cookieUtil.getCookie('usertoken');
		if(token && token.trim().length){
                        config.id.person_name.innerText = cookieUtil.getCookie('username');
			showActivity('activity-tracker-list');
			config.id.page_container.classList.add("loggedIn");
			config.id.page_container.classList.remove("loggedOut");
                        _UA_FIREWALL_KEY = token;
		}
                else{
			config.id.page_container.classList.add("loggedOut");
			config.id.page_container.classList.remove("loggedIn");
			config.id.person_name.innerText = "";
                        toggleAPIKey();
		}
	}
	
	function renderTrackerItemOnTop(item){
		item.index = ++trackersCount;
		item.img_url = BASE_URL + '/public/read/u/'+userId+'/t/'+item.id+'"/&gt;</code>';
		list_cont = list_item(item);
		let elem = config.classes.listViewHolder[0];
		elem.insertAdjacentHTML('afterbegin',list_cont);
	}

	const ulgebra = {
		init : function(){
                        this.HTTPUtil = new HTTPUtil();
			cookie_page_changes();
			console.log(_UA_FIREWALL_KEY);
			if(_UA_FIREWALL_KEY && _UA_FIREWALL_KEY.length){
				ulgebra.get_trackers();
			}
			else{
				showSignUpActivity();
			}
			ulgebra.initDomListeners();
		},
		get_user_data : function(){
			// let data = {UA_CURRENT_USER : this.get_cookie('usertoken')};
			_UA_FIREWALL_KEY = this.get_cookie('usertoken');
			if(_UA_FIREWALL_KEY ){
				this.HTTPUtil.httpGET("/me",function(resp){
					if(resp.id.length){
						let fn = resp.user.firstName ? resp.firstName : "";
							ln = resp.user.lastName ? resp.lastName : "";
							new CookieUtil().setCookie('userid',  resp.user.id  ,90);
							userId = resp.id;
						config.id.person_name.innerText = fn + " "+ ln;
					}
				},function(e){
					console.log(e);
				});
			}else{
				config.id.person_name.innerText = "";
			}	
		},
		get_cookie : function(name = ''){
			let data = new CookieUtil().getCookie(name);
			return data && data.length ? data : null;
		},
		initDomListeners : function(){
			config.id.logout_id.addEventListener('click',function(){
				new CookieUtil().eraseCookie('usertoken');
				new CookieUtil().eraseCookie('userid');
				cookie_page_changes();
			});

		
	
		},

		set_trackers_actions : function(){
			let uid = ulgebra.get_cookie('userid');
			if( ! uid || config.classes.listViewHolder[0].classList.contains("added_list")) return;

			$(document).on('click','.list-action-item.delete:not(.apiKeyUsageItem)',function(e){
				let elem = e.target;
				let tid = elem.getAttribute('data-id');
				new HTTPUtil().httpDELETE('/trackers/'+tid,function(){
					
					document.getElementById("tracker-"+tid).remove();
					
				},function(){alert('try again')});
				console.log(2);
			});

			$(document).on('click','.list-action-item.snippet',function(e){
				let elem = e.target;
				let tid = elem.getAttribute('data-id');
				console.log(tid)
				config.id.tracker_add.style.display = "none";
				config.id.tracker_succ.style.display = "block"; 
				document.getElementById("tracker-code-popup").style.display = 'block';
				config.id.card_title.innerText =  elem.getAttribute('data-name');
				let img_tok = '<code>&lt;img src="'+BASE_URL+'/public/read/u/'+uid+'/t/'+tid+'"/&gt;</code>';
				config.id.tracker_html_content.innerHTML = img_tok;
				

			})
			if( ! config.classes.listViewHolder[0].classList.contains("added_list")){
				config.classes.listViewHolder[0].classList.add("added_list")
			}
		},
		get_trackers : function(){

			
			new HTTPUtil().httpGET("/trackers",function(resp){
				
				if(resp.items && resp.items.length){
					config.classes.listViewHolder[0].innerHTML = "";
					resp.items.forEach(function(item){
						renderTrackerItemOnTop(item);
					});
					
					ulgebra.set_trackers_actions();

				}
			},function(){

			});
		}
	};

if (document.readyState!='loading') ulgebra.init();
// modern browsers
else if (document.addEventListener) document.addEventListener('DOMContentLoaded', ulgebra.init);

function findIfMobile(userAgent){
	userAgent = userAgent.toLowerCase();
	var phoneStrings = ["iphone", "android", "blackberry", "nokia", "symbian", "windows phone", "mobile", "phone"];
        for(var i=0;i<phoneStrings.length;i++){
            if(userAgent.indexOf(phoneStrings[i])>-1){
                return true;
            }
        }
        return false;
}

function getAPIKeyUsageItemFromTemplate(data, index){
	return `<div class="list-item" id="apikeyusage-${data.id}">
    <div class="list-cont-type">
        ${data.service}
    </div>
    <div class="list-content" >
        Extension Org ID : ${data.org_id}
    </div>
    <div class="list-actions">
        <div class="list-action-item apiKeyUsageItem delete" title="Delete" onclick="deleteAPIKeyUsage('${data.id}')">
            <i class="material-icons" data-id="${data.id}" data-name="${data.name}" >delete_forever</i>
        </div>
    </div>
    </div>`;
}

function getHistoryItemFromTemplate(data, index){
	var accessTime = data.hasOwnProperty('UA_RR_TIME') ? new Date(data.UA_RR_TIME).toString() : null;
	var isMobile = findIfMobile(data['user-agent']);
	return `<div class="history-item ${data.UA_RR_MARK_READ ? 'read-item-history' : ''}">
				<div class="history-top-details">
                                <div class="history-item-index">
					${index}
				</div>
				<div class="history-item-device">
					<i class="material-icons">${data.UA_RR_MARK_READ ? 'done_all' : 'done'}</i>
				</div>
				    <div class="history-item-status">
					<i class="material-icons">${isMobile ? 'phone_android' : 'desktop_mac'}</i>
				</div>
                                ${data['accept-language'] ? 
                                    `<div class="history-item-language">
					  ${data['accept-language'].split(',')[0]}
                                    </div>` : ''
                                }
				<div class="history-item-country">
					${data['cf-ipcountry']}
				</div>
				<div class="history-item-time">
					<i class="material-icons">access_time</i> ${accessTime}
				</div>
			</div>
			<div class="history-bottom-details">
                                ${data.referer ?
                                    `<div class="history-item-referrer-url">
                                            <div class="history-item-label">Accessed from</div>
                                            <a href="${data.referer}" target="_blank">${data.referer}</a>
                                    </div>` :''
                                }
				<div class="history-item-useragent">
					<div class="history-item-label">Device details</div>
					${data['user-agent']}
				</div>
                                <div class="history-item-useragent">
					<div class="history-item-label">IP Address</div>
					${data['x-forwarded-for']}
				</div>
			</div>
			</div>`;
}